#pragma once
#include "..\CSC8503Common\GameObject.h"

namespace NCL {
	namespace CSC8503 {
		class PlayerObject : public GameObject {
		public:
			PlayerObject(string name);
			void OnCollisionBegin(GameObject* otherObject) override;
			bool hitGoal = false;
			bool hitRobot = false;
		};
	}
}