#pragma once
#include "GameTechRenderer.h"
#include "../CSC8503Common/PhysicsSystem.h"
#include "../CSC8503Common/StateMachine.h"
#include "../CSC8503Common/StateTransition.h"
#include "../CSC8503Common/State.h"
#include "PlayerObject.h"
#include "NetworkedGame.h"
#include "HighScore.h"

namespace NCL {
	namespace CSC8503 {

		enum class Network { Server, Client, NotApplicable };
		enum class state { menu, level1, level2 };

		class TutorialGame		{

		public:

			TutorialGame(NetworkedGame* n);
			~TutorialGame();

			virtual void UpdateGame(float dt);

			GameWorld* world;
			GameTechRenderer*	renderer;
			PhysicsSystem*		physics;
			PlayerObject* sphere;
			GameObject* goal;
			GameObject* robot;

			HighScore* hs = new HighScore;

			NetworkedGame* network;
			Vector3 force;
			Vector3 rayPos;
			int stroke = 0;
			void LevelLoader(int level);
			void InitWorld();
		protected:

			void InitialiseAssets();

			void InitCamera();

			void UpdateKeys();

			

			

			/*
			These are some of the world/object creation functions I created when testing the functionality
			in the module. Feel free to mess around with them to see different objects being created in different
			test scenarios (constraints, collision types, and so on). 
			*/
			void InitSphereGridWorld(int numRows, int numCols, float rowSpacing, float colSpacing, float radius);
			void InitMixedGridWorld(int numRows, int numCols, float rowSpacing, float colSpacing);
			void InitCubeGridWorld(int numRows, int numCols, float rowSpacing, float colSpacing, const Vector3& cubeDims);
			void InitSphereCollisionTorqueTest();
			void InitCubeCollisionTorqueTest();
			void InitSphereAABBTest();
			void InitGJKWorld();
			void BridgeConstraintTest();
			void SimpleGJKTest();
			void SimpleAABBTest();
			void SimpleAABBTest2();
			//void CameraLookAt(Vector3 target);

			bool SelectObject();
			void MoveSelectedObject();

			GameObject* AddFloorToWorld(const Vector3& position);
			GameObject* AddSphereToWorld(const ::NCL::Maths::Vector3& position, float radius, float inverseMass, string name);
			GameObject* AddCubeToWorld(const Vector3& position, Vector3 dimensions, float inverseMass, string name);
			GameObject* AddOBBToWorld(const Vector3& position, Vector3 dimensions, float inverseMass, string name);

			void AddConstrainToWorld(Vector3 s);
			
			float rotCamera = 0.0f;
			float heightCamera = 20.0f;
			float distCamera = 10.0f;
			
			bool freeCam = false;
			bool useGravity;
			bool inSelectionMode;
			

			float		forceMagnitude;

			GameObject* selectionObject = nullptr;

			OGLMesh*	cubeMesh	= nullptr;
			OGLMesh*	sphereMesh	= nullptr;

			OGLTexture* basicTex	= nullptr;

			OGLTexture* SphereTex	= nullptr;
			OGLTexture* WallTex	= nullptr;
			OGLTexture* RobotTex = nullptr;
			OGLTexture* FloorTex = nullptr;
			OGLTexture* SpinningTex = nullptr;
			OGLTexture* GoalTex = nullptr;


			OGLShader*	basicShader = nullptr;
		};
	}
}

