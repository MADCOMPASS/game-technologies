#include "PlayerObject.h"


NCL::CSC8503::PlayerObject::PlayerObject(string name) : GameObject(name)
{
	
}

void NCL::CSC8503::PlayerObject::OnCollisionBegin(GameObject* otherObject)
{
	if (otherObject->GetName() == "goal")
	{
		hitGoal = true;
	}

	if(otherObject->GetName() == "robot")
	{
		hitRobot = true;
	}

}
