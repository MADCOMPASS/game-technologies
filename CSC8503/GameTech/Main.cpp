#include "../../Common/Window.h"

#include "../CSC8503Common/StateMachine.h"
#include "../CSC8503Common/StateTransition.h"
#include "../CSC8503Common/State.h"

#include "../CSC8503Common/GameServer.h"
#include "../CSC8503Common/GameClient.h"

#include "../CSC8503Common/NavigationGrid.h"

#include "TutorialGame.h"
#include "NetworkedGame.h"

#include "../CSC8503Common/CollisionDetection.h"
#include "NetworkPlayer.h"
#include "HighScore.h"

using namespace NCL;
using namespace CSC8503;

// stuff for the state machine
int selections[4] = { 0,1,1,1 };
state current = state::menu;
bool load1 = false;
bool load2 = false;
int counter = 300;

// global variables for path finding....
vector<Vector3> testNodes;
bool pathFound = false;
bool reset = false;
GameObject* sphere;
GameObject* robot;

void StateMachineFunc(StateMachine& testMachine, TutorialGame& tut) {

	StateFunc MainMenu = [](void * data) {
		
		TutorialGame* tut = (TutorialGame*)data;

		if (Window::GetKeyboard()->KeyPressed(KEYBOARD_DOWN)) {
			for (int i = 0; i < 4; i++)
			{
				if (selections[i] == 0)
				{
					selections[i] = 1;
					if (i == 3)
					{
						selections[0] = 0;
						selections[i] = 1;
					} else
					{
						selections[i + 1] = 0;
					}
					break;
				}
			}
		}

		if (Window::GetKeyboard()->KeyPressed(KEYBOARD_UP)) {
			for (int i = 0; i < 4; i++)
			{
				if (selections[i] == 0)
				{
					selections[i] = 1;
					if (i == 0)
					{
						selections[3] = 0;
						selections[i] = 1;
					}
					else
					{
						selections[i - 1] = 0;
					}
					break;
				}
			}
		}

		if (Window::GetKeyboard()->KeyPressed(KEYBOARD_RETURN)) {

			for (int i = 0; i < 2; i++)
			{
			if (selections[0] == 0)
			{
				current = state::level1;
			}
			if (selections[1] == 0)
			{
				current = state::level2;
			}
			}
		}

		Debug::Print("Level 1", Vector2(480, 600),Vector4(selections[0],0,1,1));
		Debug::Print("Level 2", Vector2(480, 500), Vector4(selections[1], 0, 1, 1));
		Debug::Print("Host", Vector2(480, 400), Vector4(selections[2], 0, 1, 1));
		Debug::Print("Connect", Vector2(480, 300), Vector4(selections[3], 0, 1, 1));

	};

	StateFunc Level1 = [](void * data)
	{
		load2 = false;
		TutorialGame* tut = (TutorialGame*)data;

		if (!load1) {
			counter = 300;
			tut->physics->useBroadPhase = true;
			tut->LevelLoader(1);
			load1 = true;
		}
		if (Window::GetKeyboard()->KeyPressed(KEYBOARD_M)) 
		{
			tut->world->Clear();
			tut->physics->Clear();
			current = state::menu;
		}

		if (Window::GetKeyboard()->KeyPressed(KEYBOARD_R))
		{
			tut->world->Clear();
			tut->physics->Clear();
			tut->stroke = 0;
			load1 = false;
		}

		
			if (tut->sphere->hitGoal)
			{
				//current = level2;
				string strokes = "TOTAL SCORE IS: " + std::to_string(tut->stroke); // change this to real time
				Debug::Print(strokes, Vector2(400, 300), Vector4(0, 0, 0, 1));
				// make it impossible to hit the ball
				if (counter <= 0)
				{
					current = state::level2;
					tut->world->Clear();
					tut->physics->Clear();
					tut->hs->UpdateFirstHighScore(tut->stroke);
					tut->stroke = 0;
					
				}
				counter--;

			
		}
	
	};

	StateFunc Level2 = [](void * data)
	{
		load1 = false;
		TutorialGame* tut = (TutorialGame*)data;
		
		if (!load2) {
			//tut->physics->useBroadPhase = true;
			//pathFound = true;
			tut->LevelLoader(2);
			load2 = true;
			counter = 300;
		}
		if (Window::GetKeyboard()->KeyPressed(KEYBOARD_M)) 
		{
			tut->world->Clear();
			tut->physics->Clear();
			current = state::menu;
		}

		if (Window::GetKeyboard()->KeyPressed(KEYBOARD_R) || reset)
		{
			tut->world->Clear();
			tut->physics->Clear();
			tut->stroke = 0;
			reset = false;
			load2 = false;
			pathFound = false;
		}

		if (tut->sphere->hitGoal)
		{
			string strokes = "TOTAL SCORE IS: " + std::to_string(tut->stroke); // change this to real time
			Debug::Print(strokes, Vector2(400, 300), Vector4(0, 0, 0, 1));
			if (counter <= 0)
			{
				current = state::menu;
				tut->hs->UpdateSecondHighScore(tut->stroke);
				tut->stroke = 0;
				tut->world->Clear();
				tut->physics->Clear();
			}
			counter--;
		}

		

	};

	GenericState* StateMenu = new GenericState(MainMenu, (void*)&tut);
	GenericState* StateLevel1 = new GenericState(Level1, (void*)&tut);
	GenericState* StateLevel2 = new GenericState(Level2, (void*)&tut);
	
	testMachine.AddState(StateMenu);
	testMachine.AddState(StateLevel1);
	testMachine.AddState(StateLevel2);

	GenericTransition<state&, state>* transitionA = new GenericTransition <state&, state> (GenericTransition<state&, state>::EqualsTransition, current, state::level1, StateMenu, StateLevel1); 
	GenericTransition<state&, state>* transitionB = new GenericTransition <state&, state> (GenericTransition<state&, state>::EqualsTransition, current, state::level2, StateMenu, StateLevel2);
	GenericTransition<state&, state>* transitionC = new GenericTransition <state&, state> (GenericTransition<state&, state>::EqualsTransition, current, state::menu, StateLevel1, StateMenu);
	GenericTransition<state&, state>* transitionD = new GenericTransition <state&, state> (GenericTransition<state&, state>::EqualsTransition, current, state::menu, StateLevel2, StateMenu);
	GenericTransition<state&, state>* transitionE = new GenericTransition <state&, state>(GenericTransition<state&, state>::EqualsTransition, current, state::level2, StateLevel1, StateLevel2);
	GenericTransition<state&, state>* transitionF = new GenericTransition <state&, state>(GenericTransition<state&, state>::EqualsTransition, current, state::menu, StateLevel2, StateMenu);

	// add the transitions into the state machine
	 testMachine.AddTransition(transitionA);
	 testMachine.AddTransition(transitionB);
	 testMachine.AddTransition(transitionC);
	 testMachine.AddTransition(transitionD);
	 testMachine.AddTransition(transitionE);
	 testMachine.AddTransition(transitionF);
}

void PathfindingFunc(GameWorld*& world) {

	NavigationGrid grid("TestGrid2.txt");

	NavigationPath outPath;

	Vector3 startPos;
	Vector3 endPos;

	// some hacky hacky dirty dirty stuff plz ignore
	GameObject* robot = nullptr;
	GameObject* sphere = nullptr;
	for (int i = 0; i < world->gameObjects.size(); i++)
	{
		if (world->gameObjects.at(i)->GetName() == "sphere")
		{
			sphere = world->gameObjects.at(i);
			startPos = world->gameObjects.at(i)->GetTransform().GetWorldPosition();
			sphere = world->gameObjects.at(i);
		}

		if (world->gameObjects.at(i)->GetName() == "robot")
		{
			robot = world->gameObjects.at(i);
			endPos = world->gameObjects.at(i)->GetTransform().GetWorldPosition();
			robot = world->gameObjects.at(i);
		}
	}
	

	bool found = grid.FindPath(startPos, endPos, outPath);

	Debug::DrawLine(startPos, startPos + Vector3(0, 10, 0), Vector4(1, 0, 0, 1));
	Debug::DrawLine(endPos, endPos + Vector3(0, 10, 0), Vector4(1, 0, 1, 1));

	//std::cout << ballPos << std::endl;

	testNodes.clear();

	// indicating collision between the robot and the ball
	float x = Vector3(startPos - endPos).Length();
	//float y = sphere->GetPhysicsObject()->GetLinearVelocity().Length();

	if (sphere != nullptr && robot != nullptr) {
		if (x < 18 && (sphere->GetPhysicsObject()->GetLinearVelocity().Length() > 30))
		{
			robot->GetTransform().SetWorldPosition(Vector3(0, -10000, 0));
			pathFound = false;

		}
		else if (x < 18)
		{
			reset = true;
		}

	}

	//robot moving
	if (robot != nullptr) {
		Vector3 pos;
		while (outPath.PopWaypoint(pos)) {
			pos.y = -80;
			testNodes.push_back(pos);
		}

		// following nodes
		if (testNodes.size() > 1) {
			robot->SetVelocity(testNodes.at(testNodes.size()-2));
		}

		// going straight for the ball after reaching to the last node
		else if (testNodes.size() == 1)
		{
			robot->SetVelocity(sphere->GetTransform().GetWorldPosition());
		}
	}
	 

}

void DisplayPathfinding() {

	for (int i = 1; i < testNodes.size(); ++i) {
		 Vector3 a = testNodes[i - 1];
		 Vector3 b = testNodes[i];
		// a.y = -80;
		// b.y = -80;
		 Debug::DrawLine(a, b, Vector4(0, 1, 0, 1));
	}

}

int waiting = 0; // give some frame time for the game to load up objects

int main() {
	Window*w = Window::CreateGameWindow("CSC8503 Game technology!", 1280, 720);

	if (!w->HasInitialised()) {
		return -1;
	}

	HighScore* score = new HighScore;
	StateMachine* stateMachine = new StateMachine();
	NetworkedGame* networkGame = nullptr;
	TutorialGame* g = new TutorialGame(networkGame);

	StateMachineFunc(*stateMachine, *g);

	w->ShowOSPointer(false);
	w->LockMouseToWindow(true);

	while (w->UpdateWindow() && !Window::GetKeyboard()->KeyDown(KEYBOARD_ESCAPE)) {
		float dt = w->GetTimer()->GetTimeDelta() / 1000.0f;
		
		if (dt > 1.0f) {
			continue; //must have hit a breakpoint or something to have a 1 second frame time!
		}
		if (Window::GetKeyboard()->KeyPressed(KEYBOARD_PRIOR)) {
			w->ShowConsole(true);
		}
		if (Window::GetKeyboard()->KeyPressed(KEYBOARD_NEXT)) {
			w->ShowConsole(false);
		}
		if (Window::GetKeyboard()->KeyPressed(KEYBOARD_RETURN))
		{
			if (selections[2] == 0) {
				networkGame = new NetworkedGame(*g, Network::Server);
			}
			if (selections[3] == 0) {
				networkGame = new NetworkedGame(*g, Network::Client);
			}
		}

		if (g->sphere != nullptr && g->robot != nullptr) {
			pathFound = (g->sphere->GetTransform().GetWorldPosition() - g->robot->GetTransform().GetWorldPosition()).
				Length() < 180;
		}

		if (pathFound) {
			PathfindingFunc(g->world);
			Debug::Print("ROBOT IS ACTIVE, RUN! OR ???", Vector2(350, 650), Vector4(0, 0, 0, 1));
			
		} else if(current == state::level2)
		{
			Debug::Print("Robot is quiet", Vector2(350, 650), Vector4(0, 0, 0, 1));
		}

		w->SetTitle("Gametech frame time:" + std::to_string(1000.0f * dt));
		stateMachine->Update();
		g->UpdateGame(dt);

		//updating the network and the state-machine 
		if (networkGame != nullptr)
		{
			networkGame->Update();
			if (networkGame->clientConnected || networkGame->serverConnected) {
				current = state::level1;
				if (waiting > 10) {
					networkGame->level1 = true;
					networkGame->getLocalObject();
				}
				waiting++;
			}
		}
	}
	Window::DestroyGameWindow();
	NetworkBase::Destroy();
}