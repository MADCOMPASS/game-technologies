#include "HighScore.h"
#include "../CSC8503Common/Debug.h"


HighScore::HighScore()
{
}

void HighScore::PrintHighScore()
{
	std::string score = "Current HS Level1: " + std::to_string(highscore);
	std::string score2 = "Current HS Level2: " + std::to_string(highscore2);
	NCL::Debug::Print(score, NCL::Vector2(600, 600), NCL::Vector4(1, 1, 1, 1));
	NCL::Debug::Print(score2, NCL::Vector2(600, 550), NCL::Vector4(1, 1, 1, 1));
}

void HighScore::UpdateFirstHighScore(int s)
{
	if(highscore == 0)
	{
		highscore = s;
	}
	else {

		if (s < highscore)
			highscore = s;
	}
}


void HighScore::UpdateSecondHighScore(int s)
{

	if (highscore2 == 0)
	{
		highscore2 = s;
	}
	else {

		if (s < highscore2)
			highscore2 = s;
	}
}