#pragma once
//#include "TutorialGame.h"
#include "../CSC8503Common/GameServer.h"
#include "../CSC8503Common/GameClient.h"

#include "./../CSC8503Common/NetworkBase.h"

using namespace NCL;
using namespace CSC8503;

#include <string>

using std::string;

namespace NCL {
	namespace CSC8503 {
		class TutorialGame;
		class GameObject;
		class GameServer;
		class GameClient;
		enum class Network;

		// all my dynamic objects
		//struct DynamicStruct {
		//	GameObject* Sphere;
		//	GameObject* Robot;
		//	GameObject* Spinning;
		//};

		class TestPacketReceiver : public PacketReceiver {
		public:
			TestPacketReceiver(string name) {
				this->name = name;
			}

			void ReceivePacket(int type, GamePacket* payload, int source = -1) override
			{
				//TODO :)
			}

			void ClientReceivePacket(int type, GamePacket* payload, DynamicStruct* game, int source) override;
		protected:
			string name;
		};



		class NetworkedGame
		{
		public:
			NetworkedGame(TutorialGame& g, Network status);
		
			void Update();
			void getLocalObject();
			void setLocalObject();
			void setStroke();

			// so the state machine can move on
			bool clientConnected = false;
			bool serverConnected = false;

			bool level1 = false;
			bool level2 = false;

			// probably will need to create a new one for the current state object
			// and another one for receiving client/host state object
			DynamicStruct* game = new DynamicStruct;
			TutorialGame* g;

		private:

			void HostNetworking();
			void ClientNetworking();

			// networking stuff
			GameServer* server;
			GameClient* client;
			
			Network Status;

			


		};
	}
}