#include "NetworkedGame.h"
#include "NetworkPlayer.h"
#include "../CSC8503Common/GameServer.h"
#include "../CSC8503Common/GameClient.h"
#include "TutorialGame.h"

using namespace NCL;
using namespace CSC8503;

NetworkedGame::NetworkedGame(TutorialGame& g, Network status) : g(&g), Status(status)
{
	NetworkBase::Initialise();
	if (Status == Network::Server)
	{
		HostNetworking();
	}

	if (Status == Network::Client)
	{
		ClientNetworking();
	}

	game->force = Vector3(0, 0, 0);
	game->rayPos = Vector3(0, 0, 0);
}




void NetworkedGame::HostNetworking()
{
	TestPacketReceiver* serverReceiver = new TestPacketReceiver("Server");

	int port = NetworkBase::GetDefaultPort();

	server = new GameServer(port, 1);

	server->RegisterPacketHandler(String, serverReceiver);
	server->RegisterPacketHandler(Ray_Cast, serverReceiver);

	std::cout << "Server is ready" << std::endl;
}

void NetworkedGame::ClientNetworking()
{
	TestPacketReceiver* clientReceiver = new TestPacketReceiver("Client");

	client = new GameClient();

	client->RegisterPacketHandler(String, clientReceiver);
	client->RegisterPacketHandler(Game_Object, clientReceiver);

	int port = NetworkBase::GetDefaultPort();

	clientConnected = client->Connect(127, 0, 0, 1, port);

	if (clientConnected)
	{
		std::cout << "Client is ready" << std::endl;
	}
	
}

void NetworkedGame::getLocalObject()
{
	
		for (int i = 0; i < g->world->gameObjects.size(); i++)
		{
			if (g->world->gameObjects.at(i)->GetName() == "sphere")
			{
				game->Sphere = g->world->gameObjects.at(i);
			}

			if (g->world->gameObjects.at(i)->GetName() == "robot")
			{
				game->Robot = g->world->gameObjects.at(i);
			}

			if (g->world->gameObjects.at(i)->GetName() == "spinning")
			{
				game->Spinning = g->world->gameObjects.at(i);
			}

			game->score = g->stroke; // just transmitting the score of both people 
		}
	
}

void NetworkedGame::setLocalObject()
{
	for (int i = 0; i < g->world->gameObjects.size(); i++)
	{
		if (g->world->gameObjects.at(i)->GetName() == "sphere")
		{
			g->world->gameObjects.at(i)->GetTransform().SetWorldPosition(game->Sphere->GetTransform().GetWorldPosition());
			g->world->gameObjects.at(i)->GetTransform().SetLocalOrientation(game->Sphere->GetTransform().GetLocalOrientation());
			g->stroke = game->score;
		}

		
	/*
		if (g->world->gameObjects.at(i)->GetName() == "spinning")
		{
			game->Spinning = g->world->gameObjects.at(i);
		}*/
	}
}

void NetworkedGame::setStroke()
{
	for (int i = 0; i < g->world->gameObjects.size(); i++)
	{
		if (g->world->gameObjects.at(i)->GetName() == "sphere")
		{
			g->world->gameObjects.at(i)->GetPhysicsObject()->AddForceAtPosition(game->force, game->rayPos);
		}
	}
}

void NetworkedGame::Update()
{
	
	setStroke(); // hitting the ball using information from the client

	if (Status == Network::Server)
	{
			// if someone has connected
			if (server->clientCount != 0)
			{
				serverConnected = true;

				// if level 1 has loaded
				if (level1) {
					
					server->SendGlobalPacket(ObjectPacket(game->Sphere->GetTransform().GetWorldPosition(), game->Sphere->GetTransform().GetWorldOrientation(),g->stroke));
				}
			}
		server->UpdateServer(game);
	}


	if(Status == Network::Client)
	{
		// updateClient should've updated the struct, setLocalObject will set the tutorialgame reference with the struct
		client->UpdateClient(game);
		setLocalObject();
	
		client->SendPacket(RayCastPacket(g->force, g->rayPos));
		g->force = Vector3(0, 0, 0);
		g->rayPos = Vector3(0, 0, 0);
		
	}
}

/*
 *
 *
 *q
 */

void TestPacketReceiver::ClientReceivePacket(int type, GamePacket* payload, DynamicStruct* game, int source) {
	if (type == String) {

		StringPacket* realPacket = (StringPacket*)payload;

		string msg = realPacket->GetStringFromData();

		std::cout << name << " received message: " << msg << std::endl;


	}

	if (type == Game_Object)
	{
		ObjectPacket* realPacket = (ObjectPacket*)payload;

		game->Sphere->GetTransform().SetWorldPosition(realPacket->sendPosition);
		game->score = realPacket->score;
		game->Sphere->GetTransform().SetLocalOrientation(realPacket->sendOrientation);
		//std::cout << realPacket->sendPosition << std::endl;
	}

	if(type == Ray_Cast)
	{
		RayCastPacket* realPacket = (RayCastPacket*)payload;
		
		//game->Sphere->GetPhysicsObject()->AddForceAtPosition(realPacket->sendforce, realPacket->sendrayPos);
		game->force = realPacket->sendforce;
		game->rayPos = realPacket->sendrayPos;
		std::cout << realPacket->sendforce << std::endl;
	}

}
