#include "PhysicsSystem.h"
#include "PhysicsObject.h"
#include "GameObject.h"
#include "CollisionDetection.h"
#include "../../Common/Quaternion.h"

#include "Constraint.h"

#include "Debug.h"

#include <functional>

using namespace NCL;
using namespace CSC8503;

const float rest = 15.0f;
int rest_frames = 1;
/*

These two variables help define the relationship between positions
and the forces that are added to objects to change those positions

*/

const float PhysicsSystem::UNIT_MULTIPLIER = 1.0f;
const float PhysicsSystem::UNIT_RECIPROCAL = 1.0f / UNIT_MULTIPLIER;

PhysicsSystem::PhysicsSystem(GameWorld& g) : gameWorld(g)	{
	applyGravity	= true;
	useBroadPhase	= true;	
	dTOffset		= 0.0f;
	globalDamping	= 0.95f;
	SetGravity(Vector3(0.0f, -700.0f, 0.0f));
}

PhysicsSystem::~PhysicsSystem()	{
}

void PhysicsSystem::SetGravity(const Vector3& g) {
	gravity = g * UNIT_MULTIPLIER;
}

/*

If the 'game' is ever reset, the PhysicsSystem must be
'cleared' to remove any old collisions that might still
be hanging around in the collision list. If your engine
is expanded to allow objects to be removed from the world,
you'll need to iterate through this collisions list to remove
any collisions they are in.

*/
void PhysicsSystem::Clear() {
	allCollisions.clear();
}

/*

This is the core of the physics engine update

*/
void PhysicsSystem::Update(float dt) {
	const float iterationDt = 1.0f / 120.0f; //Ideally we'll have 120 physics updates a second 
	//const float iterationDt = 1.0f / 60.0f; //Ideally we'll have 120 physics updates a second 
	dTOffset += dt; //We accumulate time delta here - there might be remainders from previous frame!

	int iterationCount = (int)(dTOffset / iterationDt); //And split it up here

	float subDt = dt / (float)iterationCount;	//How many seconds per iteration do we get?

	IntegrateAccel(dt); //Update accelerations from external forces

	for (int i = 0; i < iterationCount; ++i) {
		if (useBroadPhase) {
			BroadPhase();
			NarrowPhase();
		}
		else {
			BasicCollisionDetection();
		}

		//This is our simple iterative solver - 
		//we just run things multiple times, slowly moving things forward
		//and then rechecking that the constraints have been met

		int constraintIterationCount = 10;
		float constraintDt = subDt / (float)constraintIterationCount;

		for (int i = 0; i < constraintIterationCount; ++i) {
			UpdateConstraints(constraintDt);
			IntegrateVelocity(constraintDt); //update positions from new velocity changes
		}
		dTOffset -= iterationDt;
	}
	
	ClearForces();	//Once we've finished with the forces, reset them to zero
	UpdateObjectAABBs();
	UpdateCollisionList(); //Remove any old collisions
	
}

void PhysicsSystem::UpdateObjectAABBs() { 

	std::vector <GameObject*>::const_iterator first; 
	std::vector <GameObject*>::const_iterator last; 
	gameWorld.GetObjectIterators(first, last); 
	
	for (auto i = first; i != last; ++i) { 
		(*i)->UpdateBroadphaseAABB(); 
	} 
}


/*
Later on we're going to need to keep track of collisions
across multiple frames, so we store them in a set.

The first time they are added, we tell the objects they are colliding.
The frame they are to be removed, we tell them they're no longer colliding.

From this simple mechanism, we we build up gameplay interactions inside the
OnCollisionBegin / OnCollisionEnd functions (removing health when hit by a 
rocket launcher, gaining a point when the player hits the gold coin, and so on).
*/
void PhysicsSystem::UpdateCollisionList() {
	for (std::set<CollisionDetection::CollisionInfo>::iterator i = allCollisions.begin(); i != allCollisions.end(); ) {
		if ((*i).framesLeft == numCollisionFrames) {
			i->a->OnCollisionBegin(i->b);
			i->b->OnCollisionBegin(i->a);
			i->a->onCollision(i->b);
			i->b->onCollision(i->a);
		}
		(*i).framesLeft = (*i).framesLeft - 1;
		if ((*i).framesLeft < 0) {
			i->a->OnCollisionEnd(i->b);
			i->b->OnCollisionEnd(i->a);
			i = allCollisions.erase(i);
		}
		else {
			++i;
		}
	}
}

/*

This is how we'll be doing collision detection in tutorial 4.
We step thorugh every pair of objects once (the inner for loop offset 
ensures this), and determine whether they collide, and if so, add them
to the collision set for later processing. The set will guarantee that
a particular pair will only be added once, so objects colliding for
multiple frames won't flood the set with duplicates.
*/
void PhysicsSystem::BasicCollisionDetection() {
	std::vector <GameObject*>::const_iterator first;
	std::vector <GameObject*>::const_iterator last; 

	gameWorld.GetObjectIterators(first, last); 

	for (auto i = first; i != last; ++i) { 
		if ((*i)->GetPhysicsObject() == nullptr) { 
			continue; 
		} 
		for (auto j = i + 1; j != last; ++j) { 
			if ((*j)->GetPhysicsObject() == nullptr) { 
				continue; 
			} 
			CollisionDetection::CollisionInfo info; 
			if (CollisionDetection::ObjectIntersection(*i, *j, info)) { 
				//std::cout << "Collision between " << (*i)->GetName() << " and " << (*j)->GetName() << std::endl; 
				//info.framesLeft = numCollisionFrames; allCollisions.insert(info);
				ImpulseResolveCollision(*info.a, *info.b, info.point);
				info.framesLeft = numCollisionFrames;
				allCollisions.insert(info);
			} 
		} 
	}
}

/*

In tutorial 5, we start determining the correct response to a collision,
so that objects separate back out. 

*/
void PhysicsSystem::ImpulseResolveCollision(GameObject& a, GameObject& b, CollisionDetection::ContactPoint& p) const {

	// getting physics object containing phsyics information of the 2 objects
	PhysicsObject* physA = a.GetPhysicsObject();
	PhysicsObject* physB = b.GetPhysicsObject();

	Transform& transformA = a.GetTransform();
	Transform& transformB = b.GetTransform();

	// total inverse mass
	float totalMass = physA->GetInverseMass() + physB->GetInverseMass();

	if (totalMass == 0.0f)
		return;


	//Separate them out using projection
	transformA.SetWorldPosition(transformA.GetWorldPosition() - (p.normal * p.penetration*(physA->GetInverseMass() / totalMass)));
	transformB.SetWorldPosition(transformB.GetWorldPosition() + (p.normal * p.penetration*(physB->GetInverseMass() / totalMass)));

	// position of collision point relative to each object, 
	Vector3 relativeA = p.position - transformA.GetWorldPosition();
	Vector3 relativeB = p.position - transformB.GetWorldPosition();

	//we can also determine how much angular velocity the object has
	Vector3 angVelocityA = Vector3::Cross(physA->GetAngularVelocity(), relativeA);
	Vector3 angVelocityB = Vector3::Cross(physB->GetAngularVelocity(), relativeB);


	// combination of each object's linear and angular veloctiy, we can determine the velocities at which the two objects are colliding
	Vector3 fullVelocityA = physA->GetLinearVelocity() + angVelocityA;
	Vector3 fullVelocityB = physB->GetLinearVelocity() + angVelocityB;

	// thus the relative veloctiy of the collision
	Vector3 contactVelocity = fullVelocityB - fullVelocityA;

	if (Vector3::Dot(contactVelocity, p.normal) > 0)
		return;

	float impulseForce = Vector3::Dot(contactVelocity, p.normal);

	//inertia
	Vector3 inertiaA = Vector3::Cross(physA->GetInertiaTensor() * Vector3::Cross(relativeA, p.normal), relativeA);
	Vector3 inertiaB = Vector3::Cross(physB->GetInertiaTensor() * Vector3::Cross(relativeB, p.normal), relativeB);
	float angularEffect = Vector3::Dot(inertiaA + inertiaB, p.normal);

	float cRestitution = 0.3f; // disperse some kinectic energy

	float j = (-(1.0f + cRestitution) * impulseForce) / (totalMass + angularEffect);

	Vector3 fullImpulse = p.normal * j;

	physA->ApplyLinearImpulse(-fullImpulse);
	physB->ApplyLinearImpulse(fullImpulse);
	physA->ApplyAngularImpulse(Vector3::Cross(relativeA, -fullImpulse));
	physB->ApplyAngularImpulse(Vector3::Cross(relativeB, fullImpulse));
}

/*

Later, we replace the BasicCollisionDetection method with a broadphase
and a narrowphase collision detection method. In the broad phase, we
split the world up using an acceleration structure, so that we can only
compare the collisions that we absolutely need to. 

*/
void PhysicsSystem::BroadPhase() {
	QuadTree <GameObject*> tree(Vector2(1024, 1024), 7, 6); 
	std::vector <GameObject*>::const_iterator first; 
	std::vector <GameObject*>::const_iterator last; 
	
	gameWorld.GetObjectIterators(first, last); 
	
	for (auto i = first; i != last; ++i) { 
		Vector3 halfSizes; 
		if (!(*i)->GetBroadphaseAABB(halfSizes)) { 
			continue; 
		} 
		Vector3 pos = (*i)->GetConstTransform().GetWorldPosition(); 
		tree.Insert(*i, pos, halfSizes); 
	}

	tree.OperateOnContents([&](std::list<QuadTreeEntry<GameObject*>>& data) {
		CollisionDetection::CollisionInfo info;
		for (auto i = data.begin(); i != data.end(); ++i) {
			 for (auto j = std::next(i); j != data.end(); ++j) {
				 //is this pair of items already in the collision set 
				 //if the same pair is in another quadtree node together etc 
				 info.a = min((*i).object , (*j).object); 
				 info.b = max((*i).object , (*j).object);
				broadphaseCollisions.insert(info); 
			} 
		} 
	});

	//tree.DebugDraw();

}

/*

The broadphase will now only give us likely collisions, so we can now go through them,
and work out if they are truly colliding, and if so, add them into the main collision list
*/
void PhysicsSystem::NarrowPhase() {
	for (std::set<CollisionDetection::CollisionInfo >::iterator 
		i = broadphaseCollisions.begin(); 
		i != broadphaseCollisions.end(); ++i) {
		 CollisionDetection::CollisionInfo info = *i; 
		 if (CollisionDetection::ObjectIntersection(info.a, info.b, info)) {
			 info.framesLeft = numCollisionFrames; 
			 ImpulseResolveCollision(*info.a, *info.b, info.point); 
			 allCollisions.insert(info); //insert into our main set 
		 } 
	} 
}

/*
Integration of acceleration and velocity is split up, so that we can
move objects multiple times during the course of a PhysicsUpdate,
without worrying about repeated forces accumulating etc. 

This function will update both linear and angular acceleration,
based on any forces that have been accumulated in the objects during
the course of the previous game frame.
*/
void PhysicsSystem::IntegrateAccel(float dt) {
	// for going through all the objects in the world
	std::vector<GameObject*>::const_iterator first; 
	std::vector<GameObject*>::const_iterator last;
	gameWorld.GetObjectIterators(first, last);

	for (auto i = first; i != last; ++i) {
		PhysicsObject* object = (*i)->GetPhysicsObject();
		if (object == nullptr) {
			continue; // this object is not a physics object
		}

		
		 //allowing objects to be at rest, i could change it so it doesnt bounce by reseting
		// the force and veloctiy but it will be too much work so this is a better performance fix
		if (object->GetLinearVelocity().Length() < rest && object->GetForce().Length() < rest) {
			rest_frames++;
		}
		else {
			rest_frames = 1;
		}

		// makes the sphere stay at rest only after a certain number of frames and when the position is on the ground
		if (rest_frames > 30 && (*i)->GetName() == "sphere" && (*i)->GetTransform().GetWorldPosition().y < -87.0f )
		{
			continue;
		}

		

		float inverseMass = object->GetInverseMass();

		Vector3 linearVel = object->GetLinearVelocity();
		Vector3 force = object->GetForce();
		Vector3 accel = force*inverseMass;

		if (applyGravity && inverseMass > 0) {
			accel += gravity;
		}

		linearVel += accel * dt;
		object->SetLinearVelocity(linearVel);

		//angular 
		Vector3 torque = object->GetTorque();
		Vector3 angVel = object->GetAngularVelocity();

		if ((*i)->GetName() == "cube" || (*i)->GetName() == "robot")
		{
			torque = Vector3(0, 0, 0);
			angVel = Vector3(0, 0, 0);
		}
		
		object->UpdateInertiaTensor();

		Vector3 angAccel = object->GetInertiaTensor() * torque;

		angVel += angAccel * dt;

		

		object->SetAngularVelocity(angVel);
	}
}
/*
This function integrates linear and angular velocity into
position and orientation. It may be called multiple times
throughout a physics update, to slowly move the objects through
the world, looking for collisions.
*/
void PhysicsSystem::IntegrateVelocity(float dt) {
	std::vector<GameObject*>::const_iterator first;
	std::vector<GameObject*>::const_iterator last;
	gameWorld.GetObjectIterators(first, last);
	float dampingFactor = 1.0f - 0.95f;
	float frameDamping = powf(dampingFactor, dt);

	for (auto i = first; i != last; ++i) {
		PhysicsObject* object = (*i)->GetPhysicsObject();
		if (object == nullptr) {
			continue;
		}
		Transform& transform = (*i)->GetTransform();
		//position
		Vector3 position = transform.GetLocalPosition();
		Vector3 linearVel = object->GetLinearVelocity();
		position += linearVel * dt;
		transform.SetLocalPosition(position);
		transform.SetWorldPosition(position);
		// linear damping
		linearVel = linearVel * frameDamping;
		object->SetLinearVelocity(linearVel);

		Quaternion orientation = transform.GetLocalOrientation();
		Vector3 angVel = object->GetAngularVelocity(); // to 0 because my objects are AABB
		//Vector3 angVel = Vector3(0,0,0);

		if ((*i)->GetName() == "cube")
		{
			//torque = Vector3(0, 0, 0);
			angVel = Vector3(0, 0, 0);
		}
		else if ((*i)->GetName() == "spinning") {
			angVel = Vector3(0, 0.5, 0);
		}

		orientation = orientation + (Quaternion(angVel * dt * 0.5f, 0.0f) * orientation);
		orientation.Normalise();

		transform.SetLocalOrientation(orientation);

		//Damp the angular velocity too
		angVel = angVel * frameDamping;
		object->SetAngularVelocity(angVel);
		
	}
}

/*
Once we're finished with a physics update, we have to
clear out any accumulated forces, ready to receive new
ones in the next 'game' frame.
*/
void PhysicsSystem::ClearForces() {
	std::vector<GameObject*>::const_iterator first;
	std::vector<GameObject*>::const_iterator last;
	gameWorld.GetObjectIterators(first, last);

	for (auto i = first; i != last; ++i) {
		//Clear our object's forces for the next frame
		(*i)->GetPhysicsObject()->ClearForces();
	}
}


/*

As part of the final physics tutorials, we add in the ability
to constrain objects based on some extra calculation, allowing
us to model springs and ropes etc. 

*/
void PhysicsSystem::UpdateConstraints(float dt) {
	std::vector<Constraint*>::const_iterator first;
	std::vector<Constraint*>::const_iterator last;
	gameWorld.GetConstraintIterators(first, last);

	for (auto i = first; i != last; ++i) {
		(*i)->UpdateConstraint(dt);
	}
}