#pragma once
#include <enet/enet.h>
#include <map>
#include <string>
#include <iostream>
#include "../../Common/Vector3.h"
#include "../../Common/Quaternion.h"
//#include "../GameTech/NetworkedGame.h"
#include "Constraint.h"
//#include "GameObject.h"

//#include "../../Common/Assets.h"
//#include "../../Common/Maths.h"
//#include "GameObject.h"

enum BasicNetworkMessages {
	None,
	Hello,
	Message,
	String,
	Delta_State,	//1 byte per channel since the last state
	Full_State,		//Full transform etc
	Received_State, //received from a client, informs that its received packet n
	Player_Connected,
	Player_Disconnected,
	Shutdown,
	Game_Object,
	Ray_Cast
};
namespace NCL {
	namespace CSC8503 {
		class GameObject;
		struct DynamicStruct {
			GameObject* Sphere;
			GameObject* Robot;
			GameObject* Spinning;
			Maths::Vector3 force;
			Maths::Vector3 rayPos;
			int score;
		};
	}
}

struct GamePacket {
	short size;
	short type;

	GamePacket() {
		type		= BasicNetworkMessages::None;
		size		= 0;
	}

	// Every type of data we want to to send in our game is of a specific type, and of a specific size.
	GamePacket(short type) {
		this->type	= type;
		size = 0;
	}

	int GetTotalSize() {
		return sizeof(GamePacket) + size;
	}
};

struct StringPacket : public GamePacket {
	char	stringData[256]; // we can send upto 256 characters of string data

	StringPacket(const std::string& message) { // constructor to copy the message into the char type above
		type		= BasicNetworkMessages::String;
		size		= (short)message.length();

		memcpy(stringData, message.data(), size);
		// we don't use strings because it's nicer to have a single continguous set of bytes, meaning they will be part of the 
		// same block of memory. Think about the days of Winsock and why char is sent and not string.
	};

	std::string GetStringFromData() {
		std::string realString(stringData);
		realString.resize(size);
		return realString;
	}
};

struct ObjectPacket : public GamePacket
{
	int score;
	NCL::Maths::Vector3 sendPosition;
	NCL::Maths::Quaternion sendOrientation;

	ObjectPacket(NCL::Maths::Vector3 position, NCL::Maths::Quaternion orientation, int s) { // constructor to copy the message into the char type above
		type = BasicNetworkMessages::Game_Object;
		size = sizeof(NCL::Maths::Vector3) + sizeof(NCL::Maths::Quaternion) + sizeof( int);
		sendPosition = position;
		sendOrientation = orientation;
		score = s;
	}
};

struct RayCastPacket : public GamePacket
{
	NCL::Maths::Vector3 sendforce;
	NCL::Maths::Vector3 sendrayPos;

	RayCastPacket(NCL::Maths::Vector3 force, NCL::Maths::Vector3 rayPos)
	{
		type = BasicNetworkMessages::Ray_Cast;
		size = sizeof(NCL::Maths::Vector3) + sizeof(NCL::Maths::Vector3) + sizeof(int);
		sendforce = force;
		sendrayPos = rayPos;
	}
};

struct NewPlayerPacket : public GamePacket {
	int playerID;
	NewPlayerPacket(int p ) {
		type		= BasicNetworkMessages::Player_Connected;
		playerID	= p;
		size		= sizeof(int);
	}
};

struct PlayerDisconnectPacket : public GamePacket {
	int playerID;
	PlayerDisconnectPacket(int p) {
		type		= BasicNetworkMessages::Player_Disconnected;
		playerID	= p;
		size		= sizeof(int);
	}
};

class PacketReceiver {

public:
	virtual void ReceivePacket(int type, GamePacket* payload, int source = -1) = 0;
	virtual void ClientReceivePacket(int type, GamePacket* payload, NCL::CSC8503::DynamicStruct* game, int source = -1) = 0;
};


class NetworkBase	{
public:
	static void Initialise();
	static void Destroy();

	static int GetDefaultPort() {
		return 1234;
	}

	void RegisterPacketHandler(int msgID, PacketReceiver* receiver) {
		packetHandlers.insert(std::make_pair(msgID, receiver));
	}

protected:
	NetworkBase();
	~NetworkBase();

	bool ProcessPacket(GamePacket* p, int peerID = -1);
	bool ProcessPacketClient(GamePacket* p, NCL::CSC8503::DynamicStruct* game , int peerID = -1);

	// this is a special type of map that allows for a number of objects to be mapped to a single key
	// to allow multiple objects in our code to potentailly be interested in a certain packet type integer
	typedef std::multimap<int, PacketReceiver*>::const_iterator PacketHandlerIterator;

	bool GetPacketHandlers(int msgID, PacketHandlerIterator& first, PacketHandlerIterator& last) const {
		auto range = packetHandlers.equal_range(msgID);

		if (range.first == packetHandlers.end()) {
			return false; //no handlers for this message type!
		}
		first	= range.first;
		last	= range.second;
		return true;
	}

	ENetHost* netHandle;

	std::multimap<int, PacketReceiver*> packetHandlers;
};