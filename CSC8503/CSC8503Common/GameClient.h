#pragma once
#include "NetworkBase.h"
#include <stdint.h>
#include <thread>
#include <atomic>
#include "../GameTech/NetworkedGame.h"

struct GamePacket;

namespace NCL {
	namespace CSC8503 {
		struct DynamicStruct;
		class GameObject;
		class GameClient : public NetworkBase {
		public:
			GameClient();
			~GameClient();

			// takes in 5 numbers, 4 are "dotted decimal" format of the ipaddress, the last is the port number
			// this uniquely identify our gam's network traffic.
			bool Connect(uint8_t a, uint8_t b, uint8_t c, uint8_t d, int portNum);

			void SendPacket(GamePacket&  payload);
			void UpdateClient(DynamicStruct* game );


		protected:
			
		//	void ThreadedUpdate();

			ENetPeer*	netPeer;
			std::atomic<bool>	threadAlive;
			std::thread			updateThread;

		};
	}
}

