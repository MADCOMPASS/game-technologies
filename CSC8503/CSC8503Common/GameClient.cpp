#include "GameClient.h"
#include <iostream>
#include <string>


using namespace NCL;
using namespace CSC8503;

GameClient::GameClient()	{
	netHandle = enet_host_create(nullptr, 1, 1, 0, 0);
}

GameClient::~GameClient()	{
	threadAlive = false;
	updateThread.join();
	enet_host_destroy(netHandle);
}

bool GameClient::Connect(uint8_t a, uint8_t b, uint8_t c, uint8_t d, int portNum) {

	ENetAddress address; 
	address.port = portNum;

	// common for IP address to be stored as a single integer, and this is how eNet stores addresses.
	// bit shifting so it can be logically ORed
	address.host = (d << 24) | (c << 16) | (b << 8) | (a); 

	netPeer = enet_host_connect(netHandle, &address, 2, 0); 

	return netPeer != nullptr;
}

void GameClient::UpdateClient(DynamicStruct* game) {
	if (netHandle == nullptr) { 
		
	} 
	//Handle all incoming packets 
	ENetEvent event; // filled with the next event
	// shile loops which tries to pop off an event from eNet's internal queue
	// if successful then will become an event at the top
	while (enet_host_service(netHandle , &event , 0) > 0) { 

		if (event.type == ENET_EVENT_TYPE_CONNECT) { 
			std::cout << "Connected to server!" << std::endl;
			
		} 

		else if (event.type == ENET_EVENT_TYPE_RECEIVE) { 
			std::cout << "Client: Packet recieved..." << std::endl; 
			GamePacket* packet = (GamePacket*)event.packet ->data;
			ProcessPacketClient(packet, game);
		} 
		enet_packet_destroy(event.packet);
	
	} 
}

void GameClient::SendPacket(GamePacket&  payload) {

	// creating a packet, with info such as the actual info and the size of the packet
	ENetPacket* dataPacket = enet_packet_create(&payload,  payload.GetTotalSize(), 0); 

	// referece of the dataPacket Struct, and send it 
	enet_peer_send(netPeer, 0, dataPacket);
}

//void GameClient::ThreadedUpdate() {
//	while (threadAlive) {
//		UpdateClient();
//		std::this_thread::yield();
//	}
//}
