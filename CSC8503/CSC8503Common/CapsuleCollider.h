#pragma once
#include "CollisionVolume.h"
#include "../../Common/Vector3.h"

namespace NCL {
	class CapsuleCollider : CollisionVolume
	{
	public:
		CapsuleCollider(float height, float radius, float length): height(height), radius(radius), length(length){}
		float height;
		float radius;
		float length;

	};
}