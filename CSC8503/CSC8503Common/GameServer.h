#pragma once
#include <thread>
#include <atomic>

#include "NetworkBase.h"

struct GamePacket;

namespace NCL {
	namespace CSC8503 {
		class GameWorld;
		class GameServer : public NetworkBase {
		public:
			GameServer(int onPort, int maxClients);
			~GameServer();

			bool Initialise();
			void Shutdown();

			void SetGameWorld(GameWorld &g);

			void ThreadedUpdate();

			bool SendGlobalPacket(int msgID);
			bool SendGlobalPacket(GamePacket& packet);

			void BroadcastSnapshot(bool deltaFrame);
			void UpdateMinimumState();
			virtual void UpdateServer(DynamicStruct* game);

			int			clientCount = 0;
			
		protected:
			int			port;
			int			clientMax;
			
			GameWorld*	gameWorld;

			std::atomic<bool> threadAlive;

			

			std::thread updateThread;

			int incomingDataRate;
			int outgoingDataRate;

			std::map<int, int> stateIDs;
		};
	}
}
