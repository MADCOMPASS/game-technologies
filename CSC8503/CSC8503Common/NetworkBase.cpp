#include "NetworkBase.h"

NetworkBase::NetworkBase()
{
	netHandle = nullptr;
}

NetworkBase::~NetworkBase()
{
	if (netHandle) {
		enet_host_destroy(netHandle);
	}
}



bool NetworkBase::ProcessPacket(GamePacket * packet, int peerID)
{
	PacketHandlerIterator firstHandler;
	PacketHandlerIterator lastHandler; 
	
	bool canHandle = GetPacketHandlers(packet->type,  firstHandler, lastHandler); 
	//std::cout << std::to_string(packet->type) << std::endl;
	if (canHandle) { 
		
		for (auto i = firstHandler; i != lastHandler; ++i) { 
			i->second->ReceivePacket(packet->type, packet, peerID); 
		} 
		return true; 
	} 

	std::cout << __FUNCTION__ << " no handler for packet type " 
		<< packet->type << std::endl; 

	return false;
}

bool NetworkBase::ProcessPacketClient(GamePacket* packet, NCL::CSC8503::DynamicStruct* game, int peerID)
{
	PacketHandlerIterator firstHandler;
	PacketHandlerIterator lastHandler;

	bool canHandle = GetPacketHandlers(packet->type, firstHandler, lastHandler);
	//std::cout << std::to_string(packet->type) << std::endl;
	if (canHandle) {

		for (auto i = firstHandler; i != lastHandler; ++i) {
			i->second->ClientReceivePacket(packet->type, packet, game, peerID);
		}
		return true;
	}

	std::cout << __FUNCTION__ << " no handler for packet type "
		<< packet->type << std::endl;

	return false;
}

void NetworkBase::Initialise() {
	enet_initialize();
}

void NetworkBase::Destroy() {
	enet_deinitialize();
}