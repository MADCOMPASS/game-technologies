#pragma once
#include <vector>
#include <map>

namespace NCL {
	namespace CSC8503 {

		class State;
		class StateTransition;

		typedef std::multimap<State*, StateTransition*> TransitionContainer;
		typedef TransitionContainer::iterator TransitionIterator;

		class StateMachine	{
		public:
			StateMachine();
			~StateMachine();

			void AddState(State* s);
			void AddTransition(StateTransition* t);
			State* returnActiveState();
			void Update();

		protected:
			State * activeState;

			// where all the states are sotred, the first should be the default startin state
			std::vector<State*> allStates;

			TransitionContainer allTransitions;
		};
	}
}