#include "NavigationGrid.h"
#include "../../Common/Assets.h"


#include <fstream>
#include "Debug.h"

using namespace NCL;
using namespace CSC8503;

const int LEFT_NODE		= 0;
const int RIGHT_NODE	= 1;
const int TOP_NODE		= 2;
const int BOTTOM_NODE	= 3;

const char WALL_NODE	= 'x';
const char FLOOR_NODE	= '.';

NavigationGrid::NavigationGrid()	{
	nodeSize	= 0;
	gridWidth	= 0;
	gridHeight	= 0;
	allNodes	= nullptr;
}

NavigationGrid::NavigationGrid(const std::string&filename) : NavigationGrid() {
	std::ifstream infile(Assets::DATADIR + filename);

	infile >> nodeSize;
	infile >> gridWidth;
	infile >> gridHeight;

	allNodes = new GridNode[gridWidth * gridHeight];

	for (int y = 0; y < gridHeight; ++y) {
		for (int x = 0; x < gridWidth; ++x) {
			GridNode&n = allNodes[(gridWidth * y) + x];
			char type = 0;
			infile >> type;
			n.type = type;
			n.position = Vector3(x * nodeSize, 0, y * nodeSize);
		}
	}
	
	//now to build the connectivity between the nodes
	for (int y = 0; y < gridHeight; ++y) {
		for (int x = 0; x < gridWidth; ++x) {
			GridNode&n = allNodes[(gridWidth * y) + x];		

			if (y > 0) { //get the above node
				n.connected[0] = &allNodes[(gridWidth * (y - 1)) + x];
			}
			if (y < gridHeight - 1) { //get the below node
				n.connected[1] = &allNodes[(gridWidth * (y + 1)) + x];
			}
			if (x > 0) { //get left node
				n.connected[2] = &allNodes[(gridWidth * (y)) + (x - 1)];
			}
			if (x < gridWidth - 1) { //get right node
				n.connected[3] = &allNodes[(gridWidth * (y)) + (x + 1)];
			}
			for (int i = 0; i < 4; ++i) {
				if (n.connected[i]) {
					if (n.connected[i]->type == '.') {
						n.costs[i]		= 1;
					}
					if (n.connected[i]->type == 'x') {
						n.connected[i] = nullptr; //actually a wall, disconnect!
					}
				}
			}
		}	
	}
}

NavigationGrid::~NavigationGrid()	{
	delete[] allNodes;
}

/* returns true if a path can be found, false if not
 * 
 */
bool NavigationGrid::FindPath(const Vector3& from, const Vector3& to, NavigationPath& outPath) {

	// need to work out which node �from � sits in , and �to � sits in
	// takes in world space positions, but internally operates on a grid
	// so we must equates to which positions.We divide the positions we receive by home many units in size each node will be
	int fromX = (from.x / nodeSize);
	int fromZ = (from.z / nodeSize);
	int toX = (to.x / nodeSize);
	int toZ = (to.z / nodeSize);

	if (fromX < 0 || fromX > gridWidth - 1 ||
		fromZ < 0 || fromZ > gridHeight - 1) {
		return false; // outside of map region !
	}
	
	if (toX < 0 || toX > gridWidth - 1 ||
		toZ < 0 || toZ > gridHeight - 1) {
		return false; // outside of map region !
	}

	GridNode * startNode = &allNodes[(fromZ * gridWidth) + fromX];
	GridNode * endNode = &allNodes[(toZ * gridWidth) + toX];

	std::vector<GridNode*> openList;
	std::vector<GridNode*> closedList;

	Debug::DrawLine(startNode->position - Vector3(0,100,0), endNode->position - Vector3(0, 100, 0), Vector4(0, 1,1, 1));

	// the startNode with default values, as we didnt have to travel anywhere yet
	openList.emplace_back(startNode);
	startNode->f = 0;
	startNode->g = 0;
	startNode->parent = nullptr;
	GridNode * currentBestNode = nullptr;

	int iterations = 0;

	while (!openList.empty())
	{
		iterations++;
		currentBestNode = RemoveBestNode(openList);
		 
		if (currentBestNode == endNode) // we've found the path
		{
			GridNode* node = endNode;
			while (node != nullptr)
			{
				outPath.PushWaypoint(node->position); // push the node into the class that has a vector of positions to the goal

				Debug::DrawLine(node->position - Vector3(0, 100, 0), node->position - Vector3(0, 90, 0), Vector4(0.3, 0.2, 0.4, 1));


				node = node->parent;
			}
			return true; // it's true that we've found a path
		}
		else
		{
			for (int i = 0; i <4; ++i)
			{
				GridNode* neighbour = currentBestNode->connected[i];
				if (!neighbour) // might not be connected
				{
					continue;
				}
				bool inClosed = NodeInList(neighbour, closedList); // checks if neighbour is in the closedList
				if (inClosed)
				{
					continue; // already discarded this neighbour, in closedlist means it has been discarded
				}

				// calculating the f value for all 4 neighbour
				float h = Heuristic(neighbour, endNode);
				float g = currentBestNode->g + currentBestNode->costs[i];
				float f = h + g;

				bool inOpen = NodeInList(neighbour, openList); // checks if this neighbour is in the openlist

				if (!inOpen) // means this is the first time we've seen this node, otherwise it would've been discarded
				{
					openList.emplace_back((neighbour));

					Debug::DrawLine(neighbour->position - Vector3(0, 100, 0), neighbour->position - Vector3(0, 90, 0), Vector4(1, 1, 0, 1));
				}

				// is this route not in the openlist and is better than the existing best node?
				if (!inOpen || f < neighbour->f) {
					 neighbour->parent = currentBestNode;
					 neighbour->f = f;
					 neighbour->g = g;
					
				}

			}
			closedList.emplace_back(currentBestNode); // add the best node into the closedlist
		}
	}
	return false; //open list emptied out with no path!
}



bool NavigationGrid::NodeInList(GridNode* n, std::vector<GridNode*>& list) const {
	auto i = std::find(list.begin(), list.end(), n);
    return i != list.end();
}

// find the best node in the openlist, by their lowest f cost
GridNode*  NavigationGrid::RemoveBestNode(std::vector<GridNode*>& list) const {

	auto bestI = list.begin(); // iterator
	auto bestNode = *list.begin(); // the best node is stored here, current set the oone
	
	for (auto i = list.begin(); i != list.end(); ++i) { // loops through the list and compare with best node
		 if ((*i)->f < bestNode->f) {
			 bestNode = (*i);
			 bestI = i;	
		}	
	}

	 list.erase(bestI); // delete the iterator
	 return bestNode; // return it
}


float NavigationGrid::Heuristic(GridNode* hNode, GridNode* endNode) const {
	return (hNode->position - endNode->position).Length(); // Euclidean distance
}